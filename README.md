# README #

This is a short script I use to "run" (memorize) lines for Shakespeare plays.

### What is this repository for? ###

* This is a short script I use to "run" (memorize) lines for Shakespeare (and other) plays.
* Put your input files into the "scripts" subdirectory (I maintain submodules with cleaned up scripts so if you're in a show with me, just ask for the url to the submodule!)
* Specify an input file and optionally a role (if the role isn't specified in the .role file of the subdirectory containing the input file)
* The script will run through the text file, displaying all the cue lines for lines of the given role and prompting user input
* The script ignores everything but [a-zA-z] so don't worry about punctuation/spacing (but do worry about contractions).
* The script additionally ignores what it considers "stage directions" (read: anything in square brackets).
* The script will first quiz you on every line once, and then will quiz you on each line you get wrong the first time until you get it correct three times in a row.

### How do I get set up? ###

* install python3
* clone repository; cd into repo
* put your input files into scripts for example: mkdir scripts; git clone https://bitbucket.org/andrew_timmes/m4m
* Usage: lines-quiz <inputfile> [-r <role> | --role=<role>]
* Example: ./linesquiz scripts/m4m/m4m-I-ii-1.txt LUCIO

### Contribution guidelines ###

* This is a pet project but if you want to throw some PRs my way, go for it.

### Who do I talk to? ###

* Andrew Timmes (andrew.timmes@gmail.com)
